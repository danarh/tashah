package com.example.hp10.tahsahapp

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_main.*

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        bLogin.setOnClickListener({
            loginClickAction()
        })

        tSignup.setOnClickListener({
            var intent = Intent(this, SignUpActivity::class.java)
            startActivity(intent)
        })

    }

    fun loginClickAction(){
        eEmail.error = null
        ePassword.error = null
        var validation:Boolean = true

        var email = eEmail.text.toString()
        var pass = ePassword.text.toString()

        if(email.isEmpty()){
            eEmail.error = "Please check your e-mail"
            validation = false
        }
        if(pass.isEmpty()){
            ePassword.error = "Please check your password"
            validation = false
        }

        if(validation){
            var intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
        }//if

    }
}
