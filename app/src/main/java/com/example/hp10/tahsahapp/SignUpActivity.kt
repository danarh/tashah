package com.example.hp10.tahsahapp

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_sign_up.*
import kotlinx.android.synthetic.main.activity_sign_up.view.*

class SignUpActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        bSubmit.setOnClickListener({
            submitClickAction()
        })
    }

    fun submitClickAction(){
        eName.error = null
        eEmaill.error = null
        ePass.error = null
        eConfirmPassword.error = null
        var validation:Boolean = true

        var name = eName.text.toString()
        var email = eEmaill.text.toString()
        var pass = ePass.text.toString()
        var cPass = eConfirmPassword.text.toString()


        if(name.isEmpty()){
            eName.error = "Please check your name"
            validation = false
        }
        if(email.isEmpty()){
            eEmaill.error = "Please check your E-mail"
            validation = false
        }
        if(pass.isEmpty()){
            ePass.error = "Please check your password"
            validation = false
        }
        if(cPass.isEmpty()){
            eConfirmPassword.error = "Please check your password"
            validation = false
        }
        if(!pass.equals(cPass)){
            eConfirmPassword.error = "Password doesn't match"
            validation = false
        }
        if(validation){
            var intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }//if

    }
}
